/*
1. Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

    1.1 Он даёт возможность получать и отправлять данные с сервера без перезагрузки страницы
*/

const containerFilm = document.createElement("div");
document.body.appendChild(containerFilm);

//запрос на сервер
fetch("https://ajax.test-danit.com/api/swapi/films")
  .then((response) => response.json())
  .then((data) => {
    console.log(data);

    data.forEach((film) => {
      let elementFilm = document.createElement("div");
      elementFilm.innerHTML = `<h3 class = "film-title">Эпизод ${film.episodeId}: ${film.name}</h3>
            <p>Содержание: ${film.openingCrawl}</p>
            
            
            <div class = "loading-animation"> 
            <span class = "one"></span>
            <span class = "two"></span>
            <span class = "three"></span>
            <span class = "four"></span>
            </div>
            

            <ul class = "characters-list"></ul>
            `;
      containerFilm.appendChild(elementFilm);

      const animation = elementFilm.querySelector(".loading-animation");

      const containerCharacters = elementFilm.querySelector(".characters-list");
      const charactersMovie = film.characters.map(function (characterUrl) {
        return fetch(characterUrl).then((response) => response.json());
      });

      animation.style.display = "block";

      Promise.all(charactersMovie).then((characters) => {
        animation.style.display = "none";

        characters.forEach((character) => {
          const elementCharacter = document.createElement("li");
          elementCharacter.textContent = character.name;
          containerCharacters.appendChild(elementCharacter);
        });
      });
    });
  });